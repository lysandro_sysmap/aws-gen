// export user=242467938
// export password=sua_senha

const fs = require("fs");
const fetch = require("node-fetch");
const puppeteer = require("puppeteer");

(async () => {
  console.log(process.argv)
  const envExecution = process.argv[2];
  if(!envExecution) {
    console.warn('voce precisa informar o envExecution');
    return;
  }
  console.log(`Environemnt execution: ${envExecution}`);

  const roles = {
    general: `commercial-debug-app-write-${envExecution}`,
    s3: `commercial-s3-read-${envExecution}`,
    sqs: `commercial-sqs-read-${envExecution}`
  }
  
  const secArgumentExecution = process.argv[3];
  if(!secArgumentExecution) {
    console.warn('voce precisa informar o profile');
    return;
  }

  const role =  secArgumentExecution ? roles[secArgumentExecution] : roles.general;
  console.log(`Profile execution: ${role}`);

  try {
    const browser = await puppeteer.launch({ headless: true }); //set false if you wanna see chrome
    const page = await browser.newPage();
    await page.goto("https://devops.naturaeco.com/login");

    if (!process.env.user || !process.env.password) {
      throw Error('define a user and password env variable');
    }

    await page.type("#codCN", process.env.user);
    await page.type("#password", process.env.password);

    await page.click("button[type=submit]");

    await page.waitForTimeout(5000); // await sessionStorage be defined
    const accessToken = await page.evaluate(() =>
      window.sessionStorage.getItem("@devops-fe/accessToken")
    );
    await browser.close();

    const res = await fetch(
      "https://cloud-platform-prd-devops-api-int.mgt.naturacloud.com/v1/vault/roles/AWS",
      {
        headers: {
          accept: "application/json",
          "accept-language": "en-US,en;q=0.9,pt-BR;q=0.8,pt;q=0.7,es;q=0.6",
          access_token: accessToken,
          client_id: "329d8e1b-f655-4766-a71f-13baf9235b7f",
          "content-type": "application/json;charset=UTF-8",
          project_id: "382b9886-ac32-4ba1-abd7-0db4bbde2116",
          role: role,
          "sec-ch-ua":
            '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site",
          user_id: "242467938",
        },
        referrer: "https://platform.prd.naturacloud.com/",
        referrerPolicy: "strict-origin-when-cross-origin",
        body: '{"reason":"cred"}',
        method: "POST",
        mode: "cors",
      }
    );

    const payload = await Buffer.from(res.body.read(), "utf8");
    const cred = JSON.parse(payload.toString("utf8"));

    const filesContent = `[default]
output = json
region = us-east-1
aws_access_key_id = ${cred.access_key}
aws_secret_access_key = ${cred.secret_key}
aws_session_token = ${cred.security_token}
aws_security_token = ${cred.security_token}
`;

    fs.writeFile(
      process.env.HOME + "/.aws/credentials",
      filesContent,
      { mode: 0765 },
      function (err) {
        if (err) throw err;
        console.log("Your credential was updated.");
      }
    );
  } catch (e) {
    console.error("deu ruim ", e);
  }
})();
